#!/bin/sh

# Set Monitors
/home/lee/.config/screenlayout.sh

# compositor
killall picom
while pgrep -u $UID -x picom >/dev/null; do sleep 1; done
picom --config ~/.config/picom/picom.conf --experimental-backends --vsync &

 ~/.config/polybar/i3/i3poly.sh &

#bg
#nitrogen --restore &
feh --bg-fill /usr/share/backgrounds/Wallpapers/0066.jpg &
# clipmenud &
dunst &
autotiling &
xfce4-power-manager &
numlockx &
/usr/lib/kdeconnectd &
playerctrld daemon &
#setxkbmap -layout colemak &

[ ! -s ~/.config/mpd/pid ] && mpd &
/usr/lib/xfce-polkit/xfce-polkit &

#sxhkd
sxhkd -c $HOME/.config/i3/sxhkd/sxhkdrc &


