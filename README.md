My personal dotfiles

BSPWM config is a work in progress

SXHKD config is some of my preferred keybindings

POLYBAR is borrowed heavily from MAKC.  Added the Spotify module from https://github.com/mihirlad55/polybar-spotify-module

ZSH is stolen completely from Chris@Machine.  This guy is a guru. https://github.com/ChristianChiarulli

Everything else is a representation of my journey in both Linux and Window Managers in general.

Thanks for taking a peek!
