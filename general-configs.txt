# Some general config settings

# /etc/fstab

# /dev/sda3
UUID=0A2E27C62E27AA21	/windows10	ntfs      	rw,nosuid,nodev,user_id=0,group_id=0,allow_other,blksize=4096	0 0

# storage on TrueNAS
//192.168.2.33/storage	/home/lee/Storage	cifs	_netdev,nofail,username=lee,password=<passwordhere>,uid=1000	0 0
