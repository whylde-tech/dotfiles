#!/bin/bash

# Copy dotfiles to git repo

repo="$HOME/dotfiles/"

# System
#cp $HOME/.Xresources $repo
#cp $HOME/.bash_profile $repo
#cp $HOME/.bashrc $repo
#cp $HOME/.xinitrc $repo
cp $HOME/.xprofile $repo

# Scripts
cp -r $HOME/.local/scripts $repo
# Polybar
cp -r $HOME/.config/polybar/* $repo.config/polybar/

# BSPWM
cp -r $HOME/.config/bspwm/* $repo.config/bspwm

# i3-gaps
cp -r $HOME/.config/i3/* $repo.config/i3/

# Hyprland
cp -r $HOME/.config/hypr/* $repo.config/hypr

# Rofi
cp -r $HOME/.config/rofi/* $repo.config/rofi/

# Dunst
cp -r $HOME/.config/dunst/* $repo.config/dunst/

# Picom
cp -r $HOME/.config/picom/* $repo.config/picom/

# Alacritty
cp -r $HOME/.config/alacritty/* $repo.config/alacritty/

# Btop
cp -r $HOME/.config/btop/* $repo.config/btop/

# ZSH
cp -r $HOME/.config/zsh/* $repo.config/zsh/
cp $HOME/.zshrc $repo

# Discocss
cp -r $HOME/.config/discocss/* $repo.config/discocss/

# Ranger
cp -r $HOME/.config/ranger/* $repo.config/ranger/

# MPD
cp -r $HOME/.config/mpd/* $repo.config/mpd/

# Neofetch
cp -r $HOME/.config/neofetch/* $repo.config/neofetch/

# Ncmpcpp
cp -r $HOME/.config/ncmpcpp/* $repo.config/ncmpcpp/

# Zathura
cp -r $HOME/.config/zathura/* $repo.config/zathura/

# Firefox
profile="$(ls -alh $HOME/.mozilla/firefox/ | grep default-release | awk '{print $NF}')"
cp -r $HOME/.mozilla/firefox/$profile/chrome/* $repo.mozilla/firefox/XXXXX.default-release/chrome/

echo "Dotfiles copied, ready to commit/push."



