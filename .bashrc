#
# ~/.bashrc
#
#neofetch --ascii ~/.config/neofetch/chess.txt
rxfetch
eval "$(starship init bash)"
# If not running interactively, don't do anything
[[ $- != *i* ]] && return
export PATH=$PATH:/home/lee/bin
export EDITOR='/usr/bin/nvim'
export BROWSER='brave'
export TERMINAL='alacritty'
#alias ls='ls --color=auto'
alias vim='nvim'
alias ls='exa -a --icons --group-directories-first'
alias cat='bat'
alias cp='cp -i'
alias grep='grep --color-auto'
alias mv='mv -i'
alias rm='rm -i'
PS1='[\u@\h \W]\$ '
